import { PLANOS } from "../types";

const INITIAL_STATE = {
    all: [],
    selected: {},
    
  };
  
  const reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case PLANOS.getALL:
        state.all = action.planos;
        return state;
        
        case PLANOS.select:
          state.selected = action.planos;
          return state;
      default:
        return state;
    }
  };
  
  export default reducer;
  