import { getPlansService, getPlansByIdService } from "../../services/planos.service"
import {PLANOS} from "../types"

export const getPlans = () => {
    return async (dispatch) => {
       const result = await getPlansService()
       dispatch({type: PLANOS.getALL, planos:result.data})

    }     
}   

export const selectPlans = (planos) => {
    return async (dispatch) => {
       dispatch({type: PLANOS.select, planos: planos})

    }     
}   

export const getPlansById = (id) => {
    return async (dispatch) => {
        const result = await getPlansByIdService(id)
       dispatch({type: PLANOS.select, planos: result.data})

    } 
}    
    