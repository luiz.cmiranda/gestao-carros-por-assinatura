import { navigate } from "@reach/router"
import { saveAll } from "../../config/storage"
import { entryServiceLogin } from "../../services/entry.service"
import { ENTRY } from "../types"


export const entryLogin = (form) => {
    return async(dispatch) => {
        try {
            const result = await entryServiceLogin(form)
             if(result.data) {
                 saveAll(result.data)
                 dispatch({ type: ENTRY.login, data: result.data})
                 navigate('/admin')
             }
          
        } catch (error) {
            
        }
    }
}