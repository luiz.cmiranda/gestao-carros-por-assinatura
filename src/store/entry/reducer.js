import {ENTRY} from '../types'
import {getToken} from '../../config/storage'

const INITIAL_STATE = {
    entry: getToken() || {},
  };
  
  const reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ENTRY.login:
            state.auth = action.data
            return state      
      default:
        return state;
    }
  };
  
  export default reducer;