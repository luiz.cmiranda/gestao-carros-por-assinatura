// importação dos reduces
import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

import planosReducer from "./planos/reducer";
import entryReducer from "./entry/reducer"

const reducers = combineReducers({
  entry: entryReducer,
  planos: planosReducer
  
});

// midlewares de redux
const middlewares = [thunk];

// composicao que junta middlewares + ferramentas (devtools)
const compose = composeWithDevTools(applyMiddleware(...middlewares));

// criar store
const store = createStore(reducers, compose);

export default store;
