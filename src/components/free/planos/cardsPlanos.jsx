import styled from 'styled-components';
import React, {useEffect} from 'react';
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button
} from 'reactstrap';  
import Aos from "aos";
import "aos/dist/aos.css"


const CardsPlanos = ({data,goToAction}) => {

  useEffect( () => {
    Aos.init({});   
   
   }, [])
    return (
      <div  className="grids">
      <div data-aos="flip-left"
     data-aos-easing="ease-out-cubic"
     data-aos-duration="2000"> 
        <SCard>
          <CardImg src={process.env.REACT_APP_API + data.image.url} alt="imagem pacotes"/>
          <CardBody>
            <CardTitle tag="h5">{data.title}</CardTitle>
            <CardSubtitle tag="h6" className="mb-2 text-muted">R${data.price}</CardSubtitle>
            <CardText>{data.description}.</CardText>
            <SButton onClick={() =>goToAction(data)}>Saiba <i class="fas fa-plus"></i></SButton>
          </CardBody>
        </SCard>
      </div>
      </div>
    );
  };
  
  export default CardsPlanos;

  const SCard = styled(Card)`
  background-color: #E3D8D5 ;
  padding: 15px;
  margin:20px auto;
 
 `  

const SButton = styled(Button)`
border: thin solid #ff5c5c ;  
color: black!important;
background-color: white;
margin: 15px;
border-radius:10px;
border-color:black;
:hover{
  background-color: #B6808C ;

} 
`