import React, {useEffect}  from 'react';

import { 
  Jumbotron, 
  Button,   
  } from 'reactstrap';

import { navigate } from '@reach/router';
import styled from 'styled-components';
import Aos from "aos";
import "aos/dist/aos.css"

const Banner = (props) => {

  
  useEffect( () => {
    Aos.init({});   
   
   }, [])

          return (
            
            <div  className="grids">
            <div data-aos="fade-left"
              data-aos-easing="ease-in-sine"className="boxes">
             
              <SJumbotron>
                <h1 className="display-3">Bem Vinda ao Instituto de Beleza Js !</h1>
                <p className="lead">Aqui temos os melhores pacotes  de tratamentos de beleza, com preços super acessiveis.</p>
                <hr className="my-2" />
                <p>Conheça todas  opções de Pacotes e Serviços</p>
                
                <p className="lead">
                  <SButton onClick={()=>navigate('/planos')} color="dark">Pacotes e Serviços</SButton>
                </p>
              </SJumbotron>

              <P> Aqui temos os melhores tratamentos para você se sentir mais bela 
                dos pés a cabeça. Temos os melhores profissionais do mercado e usamos produtos exclusivos.
              </P>

              <div id="carouselExampleIndicators" className="carousel slide" data-bs-ride="carousel">
                <div className="carousel-indicators">
                  <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                  <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                  <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
   
                      </div>
                      <div className="carousel-inner">
                        <div className="carousel-item active">
                          <img src="https://res.cloudinary.com/djbzvkmqm/image/upload/c_scale,h_400,w_800/v1630348106/unha_vg8hpl.jpg" className="d-block w-100" alt="..."/>
                        </div>
                        <div className="carousel-item">
                          <img src="https://res.cloudinary.com/djbzvkmqm/image/upload/c_scale,h_400,w_800/v1630348221/maquiagem_hzyx7b.jpg" className="d-block w-100" alt="..."/>
                        </div>
                        <div className="carousel-item">
                          <img src="https://res.cloudinary.com/djbzvkmqm/image/upload/c_scale,h_400,w_800/v1630348221/lavatorio_lvudp3.jpg" className="d-block w-100" alt="..."/>
                        </div>

                      </div>
                      <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Previous</span>
                      </button>
                      <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Next</span>
                      </button>
                    </div>

               </div>
            </div>
         
          );
        };



export default Banner;

const SJumbotron = styled(Jumbotron)`
 background-color: #E3D8D5 ;
 padding: 15px;
 margin:40px auto;
 border-color:black;

`
const SButton = styled(Button)`
border: thin solid #ff5c5c ;  
color: black!important;
background-color: white;
margin: 15px;
border-radius:10px;
border-color:black;
:hover{
  background-color: #B6808C ;

} 
`

const P = styled.p`
font-size:50px;
text-align:center;
color: black;
font-family:Brush Script MT;
`