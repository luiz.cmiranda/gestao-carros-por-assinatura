import NavBarFree from "../navbar";
import styled from 'styled-components'
import { Container } from "reactstrap";


const Header = () => {
    return (
        <HeaderFree>
          <Container> 
            <NavBarFree/>
          </Container>  
        </HeaderFree>
    );
}

export default Header;

const HeaderFree = styled.header`
 background: #B6808C;
 border-bottom: black 3px solid;
`