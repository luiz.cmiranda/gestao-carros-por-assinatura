import React from 'react';

import '../../../assets/footer.css'


const Footer = () => {
   return<>
   
   <section className="tudo-footer">
 
  <footer className="text-center text-white" style={{"background-color": "#E4CCD3"}}>
   
    <div className="container p-4 pb-0 m-10 my-4">
     
      <section className="">
        <p className="d-flex justify-content-center dark align-items-center">
          <span className="me-3 text-dark"></span>
         
        </p>
         
        <div id="font">
          <p>MEIOS DE PAGAMENTOS</p>
        <i class="fab fa-cc-visa fa-3x " > </i> <i class="fab fa-cc-discover fa-3x"></i> <i class="fab fa-cc-mastercard fa-3x"></i> 
         <i class="fab fa-apple-pay fa-3x"></i> <i class="fab fa-cc-amex fa-3x"></i> <i class="fab fa-cc-diners-club fa-3x"></i>
         <i class="fab fa-google-pay fa-3x"></i> 
         <p>REDES SOCIAIS</p>
         <i class="fab fa-instagram fa-3x"></i><i class="fab fa-facebook fa-3x"></i>
         <p>CONTATO E ENDEREÇO</p>
         <i class="fab fa-whatsapp fa-2x"></i>(31) 9999-99999 <i class="fas fa-map-marker-alt fa-2x"></i> Rua Projeto Liniker BH MG
         </div>
        
      </section>

    </div>

    
    <div className="text-center p-3 text-black" style={{"background-color": " #B6808C "}}>
      © 2021 Copyright JS Instituto de Beleza. 
    

    </div>
 
  </footer>
 
</section>
   </>
}

export default Footer;

