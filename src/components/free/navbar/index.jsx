import styled from 'styled-components';
import {Link} from '@reach/router';

import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';

const NavBarFree = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar  light expand="md" >
        <Brand href="/">
         <img src="https://res.cloudinary.com/djbzvkmqm/image/upload/c_scale,h_70,w_70/v1630195286/Bege_e_Preto_Simples_e_Circular_Casamento_Evento_Logotipo_xzm7ro.png"alt="logo" title="logo"/> 
        </Brand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <Item>
              <NavLink tag={Link} to="/"><i class="fas fa-home"></i>Inicio</NavLink>
            </Item>
            <Item>
              <NavLink tag={Link} to="/planos">Pacotes e Serviços</NavLink>
            </Item>
            <Item>
              <NavLink tag={Link} to="/login">Login <i class="fas fa-sign-in-alt"></i></NavLink>
            </Item>
           </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default NavBarFree;

const Item = styled(NavItem)`
a {
border: thin solid #ff5c5c ;  
color:black!important;
background-color: white;
margin: 15px;
border-radius:10px;
border-color:black;
:hover{
  background-color: #E3D8D5 ;

} 
}
`;


const Brand = styled(NavbarBrand)`
flex:8;

`
















