import Navbar from './navbar';
import '../style.css'
import Main from './main';
import Sidebar from './sidebar';


const LayoutClosed = ({children, acessType, now, menus, uri}) => {
    return (
        
<div className="container-scroller">
   <Navbar acessType={acessType}/>
     <div className="container-fluid page-body-wrapper">
       <Sidebar menus={menus} uri={uri}/>   
       <Main title={now?.title || ""}>
        {children}
      </Main>
  </div>
</div>

    );
}

export default LayoutClosed;

