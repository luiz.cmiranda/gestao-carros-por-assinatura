import {useSelector} from "react-redux"
import styled from "styled-components"
import { removeUser } from "../../../config/storage"
import {navigate} from "@reach/router"


const Navbar = ({acessType}) => {
  const entry = useSelector(state=> state.entry.entry)
   
  const logout = () => {
      removeUser()
      navigate('/login')
  }



  return (
    <nav className="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div className="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
         <Sa className="navbar-brand brand-logo" href="index.html">
          <img src="https://res.cloudinary.com/djbzvkmqm/image/upload/v1630195286/Bege_e_Preto_Simples_e_Circular_Casamento_Evento_Logotipo_xzm7ro.png" alt="logo" />{" "}
         </Sa>
         </div>
         <div className="navbar-menu-wrapper d-flex align-items-center">
         <UserMenu>
         <li className="nav-item font-weight-semibold d-none d-lg-block">
          {/* {acessType || "TELA"} */}
           Olá! {entry.user.username} 
        </li>
         </UserMenu>
      <ActionMenu>
         <li className="nav-item font-weight-semibold d-none d-lg-block ">
          {/* {acessType || "TELA"} */}
           <button className="btn btn-sm btn-danger" onClick={logout}>Sair <i class="fas fa-sign-out-alt"></i></button>
        </li>
      </ActionMenu>
     </div>
  </nav>
    );
}

export default Navbar;



const Sa = styled.a`
 width:50px ;
 height:60px ;

`

const UserMenu = styled.div.attrs({
   className: "navbar-nav",
})`
flex:1;

`

const ActionMenu = styled.div.attrs({
   className: "navbar-nav",
})``