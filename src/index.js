import React from 'react'
import ReactDOM from 'react-dom'
import GlobalStyled from './assets/globalStyled'
import "./config/bootstrap"
import { Provider } from "react-redux";
import store from "./store";



import Routers from './router'


ReactDOM.render(
  <Provider store={store}>
    
    <GlobalStyled/>
    <Routers/>
  
  </Provider>,
  document.getElementById('root')
);

