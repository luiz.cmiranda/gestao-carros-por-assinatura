import { Redirect, Router } from "@reach/router"


import Login from "./views/auth/login";

// aqui ficam os componentes de rotas
import Free from "./views/free";
import closed from "./views/closed";
import { isAuthenticated } from "./config/storage";
import { useSelector } from "react-redux";
import { enumRole } from "./utils/roles";
import Error404 from "./views/erro/error404";


const Routers = () => {
  const userRole = useSelector(state => state.entry.entry.user.role.type)
  const RoleId = enumRole(userRole)


  const ClosedRouter = ({component: Component, ...rest}) => {
    if(!isAuthenticated()){
     return   <Redirect to="/login" noThrow />
    }

    if (rest.type !== RoleId){
       return <Error404/>

    } 
    return <Component {...rest}/>
  }

     return(
    <Router>
     <Login path="/login"/>
     <Free path='/*' />

     {/* middlewares projeto */}

     <ClosedRouter component={closed} 
     path='/admin/*'   
     perfil="ADMINISTRADOR" 
     type={1}/>

     <ClosedRouter 
     component={closed} 
     path='/clients/*' 
     perfil="CLIENTE" 
     type={2}/>   
        
    </Router>
 )
}
export default Routers;