
const {REACT_APP_TOKEN_KEY: tkn} = process.env

export const saveAll = (data) => localStorage.setItem(tkn, JSON.stringify(data))

export const getToken = () =>  JSON.parse(localStorage.getItem(tkn))

export const removeUser = () => localStorage.removeItem(tkn)

export const clearStorage = () => localStorage.clear()

const hasToken = () => {
    const data = getToken()
    return data?.jwt || false
}

export const isAuthenticated = () => hasToken() !== false
 
