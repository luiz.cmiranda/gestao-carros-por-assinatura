import http from '../config/http'

export const getPlansService = () => http.get("/planos")

export const getPlansByIdService = (id) => http.get(`/planos/${id}`)
