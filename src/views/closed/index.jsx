import { Router } from "@reach/router"

import Planos from './planos';
import Home from './home'
import LayoutClosed from '../../components/closed/layout'
import { useSelector } from "react-redux";
import { enumRole } from "../../utils/roles";
import Error404 from "../erro/error404";

const MenuBar = [
    {
      title: "Pagina Inicial",
      path: "/",
      component: Home,
      authorization: [1, 2],       
    },

    {
      title: "Pacotes e Serviços",
      path: "/planos",
      component: Planos,  
      authorization: [1],
    },


]


const Closed = ({location, uri, perfil}) => {
document.title = "ADMIN"    
const userRole = useSelector(state => state.entry.entry.user.role.type)
const RoleId = enumRole(userRole)

const routersAuthorized = MenuBar.filter((route) => route.authorization.includes(RoleId))

const routeNow = MenuBar.find((item) => item.path === `/${location.pathname.split("/")[2] || ""}`)


return(
    <Router>
        <LayoutClosed
         path='/' 
         acessType={perfil || ""}
         now={routeNow}
         menus={routersAuthorized}
         uri={uri}>

            {routersAuthorized.map(({component: Component, ...route}, i)=> ( 
               <Component {...route} key={i} />
            ))}

            <Error404 default/>
        </LayoutClosed>
    </Router>
   ) 
}




export default Closed;

