import { useState } from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import { entryLogin } from "../../store/entry/action";

const Login = () => {



const dispath = useDispatch()
const [form, setForm] = useState({})


const handlechange = (event) => setForm({
 ...form,
 [event.target.name]: event.target.value

})

const isNotValid = () => 
Object.keys(form).length === 0 ||
form.identifier?.length === 0 || 
form.password?.length === 0

const handleSubmit = () => dispath(entryLogin(form))


    return (
        
      
        <MainContainer>
            <WelcomeText>Bem Vinda </WelcomeText>
            <InputContainer>
                 
                <SInput 
                onChange={handlechange}
                type="email" 
                name="identifier"
                id="usuario"
                placeholder="E-mail" 
                value={form.identifier || ""} />
                 
                <SInput 
                onChange={handlechange}
                type="password" 
                name="password"
                id="senha"
                placeholder="Senha"
                value={form.password || ""} />
                
            </InputContainer>
            <ButtonContainer>
                <StyledButton
                  disabled={isNotValid()} 
                  onClick={handleSubmit}>
                  Entrar </StyledButton>
            </ButtonContainer>
            <LoginWith> LOGIN INSTITUTO JS</LoginWith>
            <HorizontalRule />
      <ForgotPassword>Cadastre-se aqui.</ForgotPassword>
    </MainContainer>
 
    );
}

export default Login;



const MainContainer = styled.div`
  display: flow-root;
  align-items:center;
  position: relative;
  flex-direction: row;
  margin-left: 470px;
  margin-top: 50px;
  height: 100vh;
  width: 30vw;
  background: rgba(214, 205, 205, 0.15);
  box-shadow: 0 8px 32px 0 #811a3d;
  backdrop-filter: blur(8.5px);
  -webkit-backdrop-filter: blur(8.5px);
  border-radius: 10px;
  color: black;
  text-transform: uppercase;
  letter-spacing: 0.4rem;
  @media only screen and (max-width: 320px) {
    width: 80vw;
    height: 90vh;
    hr {
      margin-bottom: 0.3rem;
    }
    h4 {
      font-size: small;
    }
  }
  @media only screen and (min-width: 360px) {
    width: 80vw;
    height: 90vh;
    h4 {
      font-size: small;
    }
  }
  @media only screen and (min-width: 411px) {
    width: 80vw;
    height: 90vh;
  }

  @media only screen and (min-width: 768px) {
    width: 80vw;
    height: 80vh;
  }
  @media only screen and (min-width: 1024px) {
    width: 70vw;
    height: 50vh;
  }
  @media only screen and (min-width: 1280px) {
    width: 30vw;
    height: 80vh;
  }
  @media only screen and (min-width: 2040px) {
    width: 30vw;
    height: 30vh;
  }
`;

const WelcomeText = styled.h2`
  margin: 2rem 0 2rem 0;
  margin-left: 90px;
  
`;





   
const ButtonContainer = styled.div`
  margin: 3rem 0 3rem 0;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;



const LoginWith = styled.h5`
  margin-left: 55px;

`;

const HorizontalRule = styled.hr`
  width: 100%;
  height: 0.9rem;
  border-radius: 0.8rem;
  border: none;
  background: black;
  background-color: #0e0c0c;
  margin: 1.5rem 0 1rem 0;
  backdrop-filter: blur(25px);
`;


const ForgotPassword = styled.h4`
  cursor: pointer;
  margin-left: 90px;
`;


const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  height: 20%;
  width: 100%;
`;


const StyledButton = styled.button`
  background: linear-gradient(to right, #811a3d 0%, #da0e6d 79%);
  text-transform: uppercase;
  letter-spacing: 0.2rem;
  width: 65%;
  height: 3rem;
  border: none;
  color: #110e0e;
  border-radius: 2rem;
  cursor: pointer;
  &:hover {
    display: inline-block;
    box-shadow: 0 0 0 0.2rem #811a3d;
    backdrop-filter: blur(12rem);
    border-radius: 2rem;
  }
`;

const SInput = styled.input`
background: rgba(255, 255, 255, 0.15);
  box-shadow: 0 8px 32px 0 rgba(10, 11, 20, 0.37);
  border-radius: 2rem;
  width: 80%;
  height: 3rem;
  padding: 1rem;
  border: none;
  outline: none;
  color: #3c354e;
  font-size: 1rem;
  font-weight: bold;
  &:focus {
    display: inline-block;
    box-shadow: 0 0 0 0.2rem #811a3d;
    backdrop-filter: blur(12rem);
    border-radius: 2rem;
  }
  &::placeholder {
    color: black ;
    font-weight: 100;
    font-size: 1rem;
  }
`

