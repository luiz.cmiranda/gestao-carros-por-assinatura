import styled from "styled-components";

const Error404 = () => {
    return (
        <>
        <Erro>
         <h1><i class="fas fa-exclamation-triangle"></i>Sentimos Muito!!!</h1>
         <h2>Error 404 <strong>Pagina não encontrada.</strong></h2>
         </Erro>
        </>
    );
}

export default Error404;

const Erro = styled.div`
  background-color  : #d63333;
  border-radius: 50px;
  text-align: center;
  box-shadow: 3px 3px 3px gray;
  max-width: 60%;
  
`