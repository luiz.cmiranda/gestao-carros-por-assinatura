
import React, { useState, useEffect }  from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getPlansById } from '../../store/planos/action';
import { Fade, Button, Form, FormGroup, Label, Input, Media, Container  } from 'reactstrap';
import * as R from 'ramda'
import styled from 'styled-components';
import ReCAPTCHA from "react-google-recaptcha";
import Aos from "aos";
import "aos/dist/aos.css"


const PlanosDetalhes = (id) => {
  const dispatch = useDispatch()

  function onChange ( valor )  { 
    console.log ( " valor Captcha: " ,  valor ) ;
  }

  const [fadeIn, setFadeIn] = useState(true);

  const toggle = () => setFadeIn(!fadeIn);


    const planosSelecionado = useSelector((state)=> state.planos.selected); 

    useEffect(() => {
     if(R.isEmpty(planosSelecionado)){
       dispatch(getPlansById(id))
     }
    },[planosSelecionado, dispatch, id])

    useEffect( () => {
      Aos.init({});   
     
     }, [])
    return (
      <div  className="grids">
      <div data-aos="fade-up-right"
        data-aos-offset="300"
        data-aos-easing="ease-in-sine"className="boxes"> 
      <SContainer>
      <Media>
      <Media left href="#">
        <Media src={process.env.REACT_APP_API + planosSelecionado.image.url} alt="logo" />
      </Media>
      <Media body>
        <Media heading>
          {planosSelecionado.title}
        </Media>
        {planosSelecionado.description}
      </Media>
    </Media>
    </SContainer>
 

       <SContainer>
      <SButton  onClick={toggle}>Cadastro aqui</SButton>
            <Fade in={fadeIn} tag="h5" className="mt-3">
            <Form>
        <SFormGroup>
        <Label for="examplePassword">Nome completo</Label>
        <Input type="name" name="name" id="examplename" placeholder="Por favor preencha o nome completo..." />
      </SFormGroup>
      <SFormGroup>
        <Label for="exampleEmail">E-mail</Label>
        <Input type="email" name="email" id="exampleEmail" placeholder="Por favor preencha seu e-mail..." />
      </SFormGroup>
      <SFormGroup>
        <Label for="examplePassword">Escolha a senha</Label>
        <Input type="password" name="name" id="password" placeholder="Por favor escolha sua senha..." />
      </SFormGroup>
     
      <SFormGroup>
        <Label for="exampleEmail">Telefone (Whatsapp)</Label>
        <Input type="number" name="number" id="exampleNumber" placeholder="Por favor seu telefone incluindo DDD..." />
      </SFormGroup>
      
      <SFormGroup>
        <Label for="exampleText">Digite aqui os pacotes que escolheu:</Label>
        <Input type="textarea" name="text" id="exampleText" />

        < ReCAPTCHA
        sitekey = " Sua chave do site do cliente "
        onChange = { onChange}
      />
      </SFormGroup>
    
     
      <SButton>Enviar</SButton>
    </Form>
            </Fade>

    </SContainer>        

      </div>
      </div>
    )    
}
    
  
  export default PlanosDetalhes;


const SButton = styled(Button)`
border: thin solid #ff5c5c ;  
color: black!important;
background-color: white;
margin: 15px;
border-radius:10px;
border-color:black;
:hover{
  background-color: #B6808C ;

} 
`

const SContainer = styled(Container)`
margin-top: 30px;
`

const SFormGroup = styled(FormGroup)`
margin: 10px;
color:#B6808C; 
`