import React, {useEffect} from 'react';
import { navigate } from '@reach/router';
import {useDispatch, useSelector} from 'react-redux';
import { Container } from "reactstrap";
import CardsPlanos from "../../components/free/planos/cardsPlanos";
import styled from "styled-components";
import { getPlans, selectPlans } from '../../store/planos/action';

  

const Planos = () => {
    const dispatch = useDispatch()
     useEffect(() => {
    dispatch(getPlans())
    
}, [dispatch]);

const planos = useSelector((state) => state.planos.all)

   const goToAction = (planos) => {
   
       dispatch(selectPlans(planos))
       .then(()=> navigate(`planos/${planos.id}`))
       .catch(() => console.log("foi mal")) 
   } 


    return(
       
    <Container>
       <PlanosContainer>
       {planos.map((planos)=> ( 
       <CardsPlanos key={planos.id}  data={planos} goToAction={goToAction}/>
       ))}
       </PlanosContainer>

    </Container>
    )    
}

export default Planos;


const PlanosContainer = styled.div`
padding:20px;
display: grid;
grid-template-columns: repeat(4, 1fr);
grid-gap: 10px;


`;