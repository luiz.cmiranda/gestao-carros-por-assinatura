import { Router } from "@reach/router"

import Planos from "./planos"
import Home from './home'
import LayoutFree from '../../components/free/layout'
import PlanosDetalhes from "./planosDetalhes"


const Free = () => (
    
    <Router>
    <LayoutFree path='/'>
    <Home path='/' />
     <Planos path='/planos'/>
     <PlanosDetalhes path='/planos/:id' />
     </LayoutFree>
    </Router>
    
)

export default Free;